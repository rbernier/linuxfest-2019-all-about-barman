#!/bin/bash

set -e

echo "===== CREATE USER ACCOUNT PASSWORDS ====="
# postgres password
A=$(< /dev/urandom tr -dc _A-Za-z0-9 | head -c10)

# replicant password
B=$(< /dev/urandom tr -dc _A-Za-z0-9 | head -c10)

# BARMAN replication & administration
C=$(< /dev/urandom tr -dc _A-Za-z0-9 | head -c10)



echo "===== CREATING BARMAN CONTAINER ====="
lxc-copy -n template_centos7 -N BARMAN && sleep 3s
lxc-start -n BARMAN && sleep 3s

lxc-attach -n BARMAN <<_eof_
    yum install https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-centos96-9.6-3.noarch.rpm -y
    yum update -y
    yum install barman barman-cli.noarch postgresql96 rsync sshpass -y

    echo barman > /etc/hostname
    echo barman | passwd --stdin barman
    reboot
_eof_

sleep 5s

echo "===== CREATE CONTAINER: BARMAN_pg1 ====="
lxc-copy -n template_centos7 -N BARMAN_pg1 && sleep 3s
lxc-start -n BARMAN_pg1 && sleep 3s

lxc-attach -n BARMAN_pg1 <<_eof_
    yum install https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-centos96-9.6-3.noarch.rpm -y
    yum update -y
    yum install barman-cli.noarch postgresql96-contrib postgresql96-server rsync sshpass -y
_eof_

lxc-attach -n BARMAN_pg1 <<_eof_
    echo barman_pg1 > /etc/hostname
    echo postgres | passwd --stdin postgres
    reboot
_eof_

sleep 5s

lxc-attach -n BARMAN_pg1 <<_eof_
    su - postgres <<_e1_
        echo "export PAGER=less" > .pgsql_profile

        echo "#hostname:port:database:username:password" > .pgpass
        echo "*:5432:*:postgres:$A" >> .pgpass
        echo "*:5432:*:replicant:$B" >> .pgpass
        echo "*:5432:*:streaming_barman:$C" >> .pgpass
        echo "*:5432:*:barman:$C" >> .pgpass

        ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa && sleep 3s
        echo barman | sshpass ssh-copy-id -f barman@barman && sleep 2s

        chmod 600 .pgpass
        chmod 700 .pgsql_profile
_e1_
_eof_


echo "===== CREATE OTHER CONTAINERS ====="
lxc-stop -n BARMAN_pg1 && sleep 3s
lxc-copy -n BARMAN_pg1 -N BARMAN_pg2
lxc-copy -n BARMAN_pg1 -N BARMAN_pg3


echo "===== SETUP: BARMAN_pg1 ====="
lxc-start -n BARMAN_pg1 && sleep 3s

lxc-attach -n BARMAN_pg1 <<_eof_
    export PGSETUP_INITDB_OPTIONS="--auth-host=md5"
    /usr/pgsql-9.6/bin/postgresql96-setup initdb
    systemctl enable postgresql-9.6
    systemctl start postgresql-9.6

    sleep 3s

    su - postgres <<_e1_
        echo " host    all          all                 0.0.0.0/0               md5" >> /var/lib/pgsql/9.6/data/pg_hba.conf
        echo " host    replication  replicant           0.0.0.0/0               md5" >> /var/lib/pgsql/9.6/data/pg_hba.conf
        echo " host    replication  streaming_barman    0.0.0.0/0               md5" >> /var/lib/pgsql/9.6/data/pg_hba.conf

        psql <<_e2_
            alter system set listen_addresses='*';
            alter system set wal_level='logical';
            alter system set max_wal_senders=10;
            alter system set wal_sender_timeout=600000;
            alter system set max_replication_slots=10;
            alter system set hot_standby='on';
            alter system set wal_receiver_timeout = 600000;
            alter system set archive_mode='on';
            alter system set archive_command='/usr/bin/true';

            alter role postgres with password '$A';
            drop role if exists replicant;
            create role replicant with replication login encrypted password '$B';
            create role streaming_barman with replication login encrypted password '$C';
            create role barman with login superuser encrypted password '$C';
_e2_
_e1_

    systemctl restart postgresql-9.6
    (mandb && updatedb) > /dev/null 2>&1
_eof_



echo "===== SETUP: BARMAN_pg2 ====="
lxc-start -n BARMAN_pg2 && sleep 3s

lxc-attach -n BARMAN_pg2 <<_eof_
    echo barman_pg2 > /etc/hostname
    reboot
_eof_

sleep 5s

lxc-attach -n BARMAN_pg2 <<_eof_
    su - postgres <<_e1_
        pg_basebackup -D /var/lib/pgsql/9.6/data \
                      -R -X stream -c fast  -P -v \
                      -d 'host=barman_pg1 user=replicant'
_e1_

    sleep 5s
    systemctl enable postgresql-9.6
    systemctl start postgresql-9.6
    (mandb && updatedb) > /dev/null 2>&1
_eof_


echo "===== SETUP: BARMAN ====="
lxc-attach -n BARMAN <<_eof_
    su - barman <<_e1_
        echo "hostname:port:database:username:password" > .pgpass
        echo "*:5432:*:streaming_barman:$C" >> .pgpass
        echo "*:5432:*:barman:$C" >> .pgpass

        chmod 600 .pgpass

        # create private/public key and copy over to pg servers
        # assume for example password="postgres"
        ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa && sleep 3s
        echo postgres | sshpass ssh-copy-id -f postgres@barman_pg1 && sleep 2s
        echo postgres | sshpass ssh-copy-id -f postgres@barman_pg2 && sleep 2s

        # test connection between barman and "barman_pg1"
        psql -U streaming_barman -h barman_pg1   -c "IDENTIFY_SYSTEM"   replication=1
_e1_
    (mandb && updatedb) > /dev/null 2>&1
_eof_
