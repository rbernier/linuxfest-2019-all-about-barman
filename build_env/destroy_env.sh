#!/bin/bash

set +e

(
lxc-stop -n BARMAN_pg1
lxc-stop -n BARMAN_pg2
lxc-stop -n BARMAN_pg3
lxc-stop -n BARMAN

sleep 3s

lxc-destroy -n BARMAN_pg1
lxc-destroy -n BARMAN_pg2
lxc-destroy -n BARMAN_pg3
lxc-destroy -n BARMAN
) 2>/dev/null

echo "DONE"
