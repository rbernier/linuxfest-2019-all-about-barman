# EXAMPLE INVOCATIONS EXECUTED ON HOST BARMAN

## TEST CONNECTIVITY
```
for u in 1 2
do
    echo "================== barman_pg$u =================="
    echo "test ROLE barman"
    psql -h barman_pg$u -U barman  postgres -c 'select 1 as ping'
    echo "test streaming replication configuration"
    psql -h barman_pg$u -U streaming_barman  -c 'IDENTIFY_SYSTEM' replication=1
done
```

```
barman replication-status -h
```

```
barman replication-status --minimal --target all pg1_ssh_server
```

### EXAMPLE OUTPUT OF REPLICATION STATUS
```
-bash-4.2$ barman replication-status --minimal --target all pg1_ssh_server
Streaming clients for master 'pg1_ssh_server' (LSN @ 0/3D002AE8):
  1. A) replicant@10.0.3.247 S:0/3D002AE8 F:0/3D002AE8 R:0/3D002AE8 P:0 AN:walreceiver

#############
#
psql -h barman_pg1 -c "select * from pg_xlogfile_name('0/3D002AE8')"

     pg_xlogfile_name
--------------------------
 00000001000000000000003D
#
#############

```

## BARMAN BACKUP
```
barman backup -h
```


```
barman backup all

```

## BARMAN LIST SERVER
```
barman list-server -h
```
```
barman show-server -h
```
```
for u in $(barman list-server --minimal)
do
    set +e
    echo "---- $u ---"
    barman show-server $u 2>/dev/null
done | less
```

## BARMAN CHECK
```
barman check -h
```
```
barman check all | less
```

## BARMAN LIST BACKUPS
```
barman list-backup -h
```

```
for u in $(barman list-server --minimal)
do
    echo "---- $u ---"
    barman list-backup --minimal $u
done | less
```

## BARMAN SHOW BACKUPS
```
barman show-backup -h
```

```
(for u in $(barman list-server --minimal)
do
    echo "=============================== $u ==============================="
    barman show-backup $u latest
done) | less
```

## BARMAN RECOVER
```
barman recover -h
```

```
barman  \
        recover \
        --remote-ssh-command 'ssh postgres@barman_pg3' \
        --standby-mode \
        pg1_ssh_server 20190426T200056 \
        /var/lib/pgsql/9.6/data
```

